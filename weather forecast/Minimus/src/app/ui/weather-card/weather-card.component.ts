import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WeatherService} from '../../services/weather/weather.service';
import {UiService} from '../../services/ui/ui.service';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})
export class WeatherCardComponent implements OnInit, OnDestroy {

  condition: string;
  currentTemp: number;
  maxTemp: number;
  minTemp: number;
  darkMode: boolean;

  constructor(public weather: WeatherService,
              public router: Router,
              public ui: UiService) {
  }

  ngOnInit() {
    this.ui.darkModeState.subscribe((isDark) => {
      this.darkMode = isDark;
    });

    this.weather.getWeatherState('Ramallah')
      .subscribe((data: string) => {
        this.condition = data;
      });

    this.weather.getCurrentTemp('Ramallah').subscribe((data: number) => {
      this.currentTemp = data;
    });
    this.weather.getMinTemp('Ramallah').subscribe((data: number) => {
      this.minTemp = data;
    });
    this.weather.getMaxTemp('Ramallah').subscribe((data: number) => {
      this.maxTemp = data;
    });
  }

  ngOnDestroy() {
//A callback method that performs custom clean-up, invoked immediately after a directive, pipe, or service instance is destroyed.
  }

  openDetails() {
    this.router.navigateByUrl('/details/Ramallah');
  }

}