import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WeatherService} from '../../services/weather/weather.service';
import {SearchService} from '../../services/search/search.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  city: string
  matchingCities: string[]

  constructor(public weather: WeatherService,
              public search: SearchService,
              public router: Router) {
    this.city = '';
  }

  ngOnInit() {
  }

  getMatchingCities() {
    this.matchingCities = this.search.getMatchingCities(this.city);

  }

  addCity() {

      this.weather.addCity(this.city);
      this.router.navigateByUrl('/');
    
    
  }

  updateCity(city: string) {
    this.city = city;
    if (city === '') {
      this.matchingCities = []
    } else {
      this.getMatchingCities();
    }
  }

}